from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from ppwstory.models import *
from ppwstory.views import *
from ppwstory.forms import *
from ppwstory.apps import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class ppwstoryUnitTest(TestCase):
    def test_ppwstory_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_ppwstory_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_using_create_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, create_status)

    def test_using_me_func(self):
        found = resolve('/me/')
        self.assertEqual(found.func, me_func)

    def test_index_contains_me(self):
        response = Client().get('/me/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hi! My name is Millenio", response_content)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(title='Name', description='Status')

        # Retrieving all available activity
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_index_contains_greeting(self):
        response = Client().get('/status/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, Apa Kabar?", response_content)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_ppwstory_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('/status/', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_ppwstory_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status/', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_apps(self):
        self.assertEqual(PpwstoryConfig.name, 'ppwstory')
        self.assertEqual(apps.get_app_config('ppwstory').name, 'ppwstory')

    def test_valid_input(self):
        status_form = StatusForm({'title': "Status", 'description': "description"})
        self.assertTrue(status_form.is_valid())
        status = Status()
        status.title = status_form.cleaned_data['title']
        status.description = status_form.cleaned_data['description']
        status.save()
        self.assertEqual(status.title, "Status")
        self.assertEqual(status.description, "description")

    def test_invalid_input(self):
        status_form = StatusForm({
            'description': 301 * "X"
        })
        self.assertFalse(status_form.is_valid())

    def test_single_entry(self):
        Status.objects.create(title="status")
        response = self.client.get('/status/')
        self.assertContains(response, 'status')
        self.assertEqual(Status.objects.all().count(), 1)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/status/')
        self.assertIn('', self.browser.title)

        text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Hello, Apa Kabar?', text)

        inputbox1 = self.browser.find_element_by_id('id_title')
        time.sleep(1)
        inputbox1.send_keys('Halo')
        time.sleep(2)

        inputbox2 = self.browser.find_element_by_id('id_description')
        time.sleep(1)
        inputbox2.send_keys('Namaku Pewe')
        inputbox2.send_keys(Keys.ENTER)
        time.sleep(5)

        self.assertIn('Halo', self.browser.page_source)
        self.assertIn('Namaku Pewe', self.browser.page_source)

    def test_style_header(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/status/')
        header = self.browser.find_element_by_tag_name('h1')
        self.assertIn("light-color-font", header.get_attribute('class'))

    def test_style_button(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/status/')
        button = self.browser.find_element_by_tag_name('button')
        self.assertIn("submit", button.get_attribute('type'))
        self.assertIn("btn btn-secondary btn-lg btn-block", button.get_attribute('class'))

    def test_header_inside_column(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/status/')
        self.assertTrue(self.browser.find_elements_by_css_selector('div.col-md-12 > h1.light-color-font')[0].is_displayed())

    def test_big_box(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/status/')
        self.assertTrue(self.browser.find_elements_by_css_selector('div.big-box > div.col-md-12 > div.form-group')[0].is_displayed())
