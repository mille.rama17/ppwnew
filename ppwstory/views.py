from django.shortcuts import render, redirect
from .forms import *
# from django.http import HttpResponseRedirect

# Create your views here.
def create_status(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            status = Status()
            status.title = form.cleaned_data['title']
            status.description = form.cleaned_data['description']
            status.save()
        return redirect('/status/')
    else:
        form = StatusForm()
        status = Status.objects.all()
        response = {'status' : status, 'title' : 'List', 'form': form}
        return render(request, 'index.html', response)

def me_func(request):
    response = {'title':'Me'}
    return render(request, 'me.html', response)
