from django.apps import AppConfig


class PpwstoryConfig(AppConfig):
    name = 'ppwstory'
