from django.conf.urls import url, include
from django.urls import path
from . import views
from ppwstory.models import Status


urlpatterns = [
    path('', views.create_status, name = 'create'),
    path('', views.delete_all, name = 'clear')
]
