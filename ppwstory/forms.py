from django import forms
from .models import Status

class StatusForm(forms.Form):
    title = forms.CharField(label="Status", max_length = 30, widget = forms.TextInput(
        attrs = {'placeholder':'Activity', 'required':True,}
    ))

    description = forms.CharField(label="Description", max_length = 30, widget = forms.TextInput(
        attrs = {'placeholder':'Description', 'required':True,}
    ))
