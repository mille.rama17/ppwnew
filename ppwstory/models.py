from django.db import models

# Create your models here.
class Status(models.Model):
    title = models.CharField(max_length=140)
    description = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add = True, null = True)


    def __str__(self):
        return self.title
