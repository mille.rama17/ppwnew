from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from story8.views import *
from story8.apps import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile_func)

    def test_using_check_email(self):
        found = resolve('/email_check/')
        self.assertEqual(found.func, email_validate)

    def test_using_subscribe_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe_func)

    def test_using_subscribe_add(self):
        found = resolve('/add_subscribe/')
        self.assertEqual(found.func, subscriber_add)

    def test_index_contains_greeting(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hi! My name is Millenio Ramadizsa", response_content)

    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

class story8FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(story8FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(story8FunctionalTest, self).tearDown()

    def test_change(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/')
        button = self.browser.find_element_by_id('themes')
        body = self.browser.find_element_by_css_selector('body')
        color = body.value_of_css_property('background')
        self.assertEqual("rgb(108, 92, 231) none repeat scroll 0% 0% / auto padding-box border-box", color)
        button.send_keys(Keys.ENTER)
        color2 = body.value_of_css_property('background')
        self.assertEqual("rgb(155, 89, 182) none repeat scroll 0% 0% / auto padding-box border-box", color2)

    def test_accordion(self):
        self.browser.get('http://story6-ppw-b-nio.herokuapp.com/')
        accordion = self.browser.find_elements_by_id('accd')
        text = accordion[0].text
        self.assertIn("Activity", text)
        organization = accordion[1]
        text2 = organization.text
        self.assertIn("Organization", text2)
        organization.send_keys(Keys.ENTER)
        list = self.browser.find_elements_by_tag_name('li')
        self.assertIn("Sleeping", list[0].text)
