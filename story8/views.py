from django.shortcuts import render
from .forms import *
from .models import *
from django.core.validators import validate_email
from django.core import serializers
from django.http import HttpResponse
from django.http import JsonResponse

# Create your views here.
response={}
def profile_func(request):
    response = {'title':'Profile'}
    return render(request, 'profile.html', response)

def subscribe_func(request):
    response = {'form':SubscribeForm}
    html = "subscribe.html"
    return render(request, html, response)

def email_validate(request):
    print('stdouhghj')
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Wrong Email Format'
        })
    found = Subscribe.objects.filter(email=request.POST['email'])
    if found:
        return JsonResponse({
            'message':'Email already exist',
            'status':'fail'
        })
    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })

def subscriber_add(request):
    if (request.method == "POST"):
        subscribe = Subscribe(
            name = request.POST['name'],
            email = request.POST['email'],
            password = request.POST['password']
        )
        subscribe.save()
        return JsonResponse({
            'message':'You have Subscribed to Millenio'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"Fail!"
        }, status = 403)

def get_subscriber(request):
    if request.method == 'GET':
        lst_subscriber = serializers.serialize('json', Subscribe.objects.all())
        response = lst_subscriber

    else:
        response['message'] = 'Method POST not ALLOWED HERE'
    return HttpResponse(response, content_type='application/json')

def del_subscriber(request):
    if request.method == 'POST':
        subscriber = Subscribe.objects.get(pk=request.POST.get('pk'))
        print(subscriber)
        subscriber.delete()
        response['message'] = "success"
    else:
        response['message'] = "Method GET not allowed here"
    return JsonResponse(response)
