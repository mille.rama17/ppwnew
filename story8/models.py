from django.db import models

# Create your models here.
class Subscribe(models.Model):
    name = models.CharField(max_length = 140)
    email = models.EmailField(max_length = 140)
    password = models.CharField(max_length = 140)
