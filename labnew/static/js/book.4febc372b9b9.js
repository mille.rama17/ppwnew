const debug = false

const postFavorite = (event, title) => {
  event.preventDefault(true)
  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const count = document.getElementById('count').innerText
  if (debug) console.table([token, count])
  fetch('http://localhost:8000/books', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      title: title,
      count: count,
    })
  })
  .then(res => {
    if (debug) console.log('RES:', res)
    return res.json()
  })
  .then(json => {
    if (debug) console.log('newJSON:', json)
    document.getElementById('count').innerText = json.count
    event.target.classList.add('btn-outline-success')
    event.target.classList.remove('btn-success')
    event.target.setAttribute('disabled', true)
    event.target.innerText = 'Favorited'
  })
  .catch(err => {
    console.error(err)
  })
}
