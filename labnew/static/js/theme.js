 $(document).ready(function(){
     $( "#accordion" ).accordion();
     var changed = true;
     $("#themes").click(function(){
         if(changed){
             console.log("theme changed");
             $("body").css('background', '#9b59b6');
             changed = false;
         }else{
             console.log("back to original");
             $("body").css('background', '#6c5ce7');
             changed = true;
         }

     });
     document.onreadystatechange = function(e)
     {
       if(document.readyState=="interactive")
       {
         var all = document.getElementsByTagName("*");
         for (var i=0, max=all.length; i < max; i++)
         {
           set_ele(all[i]);
         }
       }
     }

});
