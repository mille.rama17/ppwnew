var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $("#emailClass").keyup(function() {
        checkEmail();
    });

    $("#passwordClass").keyup(function() {
        $('#statusForm').html('');
        if ($('#passwordClass').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function() {
        checkEmail()
    });

    $('#submit').click(function () {
        data = {
            'name' : $('#nameClass').val(),
            'email' : $('#emailClass').val(),
            'password' : $('#passwordClass').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'add_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('nameClass').value = '';
                document.getElementById('emailClass').value = '';
                document.getElementById('passwordClass').value = '';

                $('#statusForm').html('');
                checkAll();
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('#emailClass').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    console.log('1');
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }

        }
    });
}

function checkAll() {
    if (emailIsValid &&
        $('#nameClass').val() !== '' &&
        $('#passwordClass').val() !== '' &&
        $('#passwordClass').val().length > 7) {

        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}
