$(document).ready(function() {
  $('.btn-info').click(function() {
    $.get('/jsonreq/' + $('.searchbar').val(), function(result) {
      result.items.forEach(function(book, index) {
        $('#content').append(`
          <tr>
          <th scope="row">${index+1}</th>
          <td><img src="${book.volumeInfo.imageLinks.smallThumbnail}" alt=""></td>
          <td>${book.volumeInfo.title}</td>
          <td>${book.volumeInfo.authors.join(', ')}</td>
          <td>${book.volumeInfo.publisher}</td>
          <td>${book.volumeInfo.publishedDate}</td>
          <td class="text-center">
          <img class="star"
          src="/static/assets/star(2).png"
          onmouseover='lightStar(this)'
          onmouseout='darkStar(this)'
          alt=""/>
          </td>
          </tr>
          `)
        })
      })
    })
    $('.table').on("click", ".star", starClickedNotFav)

})

function lightStar(element) {
    $(element).attr('src', '/static/assets/star(1).png')
}

function darkStar(element) {
    $(element).attr('src', '/static/assets/star(2).png')
}

function starClickedNotFav() {
  if ($(this).attr("alt") == "clicked") {
    var nextCnt = parseInt($("#favourite_count").html()) - 1
    $("#favourite_count").html(nextCnt.toString())
    darkStar(this)
    $(this).attr('onmouseover', lightStar)
    $(this).attr('onmouseout', darkStar)
    $(this).attr('alt', "")
  } else {
    var nextCnt = parseInt($("#favourite_count").html()) + 1
    $("#favourite_count").html(nextCnt.toString())
    lightStar(this)
    $(this).removeAttr('onmouseover')
    $(this).removeAttr('onmouseout')
    $(this).attr('alt', "clicked")
  }
}
