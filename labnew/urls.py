"""labnew URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from ppwstory.views import *
from story8.views import *
from story9.views import *

urlpatterns = [
        path('admin/', admin.site.urls),
        path('status/', create_status),
        path('me/', me_func),
        path('', profile_func),
        path('books/', book_func, name = "books"),
        path('login/', login, name = "login"),
        path('logout/', logoutredirect, name = "logout"),
        path('jsonreq/<str:book>', jsonreq_func),
        path('email_check/', email_validate),
        path('subscribe/', subscribe_func),
        path('add_subscribe/', subscriber_add),
        path('get_subscriber/', get_subscriber),
        path('del_subscriber/', del_subscriber),
        path('auth/', include('social_django.urls', namespace='social')),
]
