from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout
import requests
import json

def book_func(request):
    return render(request, 'books.html')

def jsonreq_func(request, book):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + book)
    return JsonResponse(response.json())

def login(request):
    return render(request, 'login.html')

def logoutredirect(request):
    logout(request)
    return redirect('login')

def books(request, search ='quilting'):
    if  request.user.is_authenticated :
        if "like" not in request.session:
            request.session["fullname"]=request.user.first_name+" "+request.user.last_name
            request.session["username"]=request.user.username
            request.session["email"] = request.user.email
            request.session["sessionid"]=request.session.session_key
            request.session["like"]=[]
        return render(request, 'books.html', {"search":search})
    else:
        return redirect('login')
