from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from story9.views import *
from story9.apps import *

# Create your tests here.
class story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_story9_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_using_books_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, book_func)

    # def test_using_jsonreq_func(self):
    #     found = resolve('/jsonreq/')
    #     self.assertEqual(found.func, jsonreq_func)

    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')
